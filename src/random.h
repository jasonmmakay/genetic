#pragma once

#include <stdlib.h>
#include <vector>


// Generate a floating point number in the range [0, 1)
float frand();

// Generate a floating point number in the range [0, 1]
float uniform();

// Generate a floating point number in the range [low, high]
float uniform(float low, float high);

// Generate an approximately normal floating point number in the range
// [low, high] by taking the average of multiple uniform samples in the
// same range
float normalish(float low, float high, size_t samples);

// Generate a random integer in the range [0, size-1]
// NOTE: certain indices may never be generated if size > RAND_MAX, if we
//  get too worried, we should use Mersenne Twister
size_t randRange(size_t size);

// Generate an integer that is, on average, equal to proportion*total where
// the result will randomly be adjusted from the truncated result of the
// product by selecting a random number and comparing with the fractional
// remainder of the product.
size_t randPart(float proportion, size_t total);


// Shuffle a vector of items with the Fisher-Yates algorithms
template <typename T>
void shuffle(std::vector<T> &items)
{
    // nothing to shuffle if there are less than two items
    if (items.size() < 2)
        return;

    // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    for (size_t idx = 0; idx < items.size() - 1; idx++)
    {
        // generate an index in [idx, n-1]
        size_t otherIdx = idx + randRange(items.size() - idx);

        // swap the items
        T& temp = items[idx];
        items[idx] = items[otherIdx];
        items[otherIdx] = temp;
    }
}


/**
 * Generate random indices as if each were written on a card and a card was
 * drawn from the top of the deck until it was depleted, at which time all
 * cards are put back into the deck, it is reshuffled and drawing begins
 * again, ensuring that no index is generated more than once more than any
 * other index.
*/
class IndexShuffleCycler
{
private:
    // the shuffled list of indices, from [0, size-1]
    std::vector<size_t> m_indices;

    // the next position of the next index to return from m_indices
    size_t m_index = 0;

public:

    // Build the list of indices for a list of size count and shuffle them
    IndexShuffleCycler(size_t count);

    // Return the next random index, shuffling the list if they have all been
    // returned already
    size_t next();
};

#include "genetic.h"
#include "algebra.h"
#include <iostream>
#include <cstring>
#include <time.h>

using namespace std;


/**
 * Create PiecewiseLines for a genetic population. Random spawns have their
 * parameters uniformly generated over their respective ranges; individuals
 * birthed from two parents set their parameters as a normalish random value
 * between the corresponding parents' parameter values.
 */
class PiecewiseLineSpawner : public Spawner<PiecewiseLine>
{
private:
    // The data points to fit. Stored in a pointer to pass on to each
    // PiecewiseLine
    shared_ptr<DataSet> m_data;

    // The number of pieces (or intervals) in each PiecewiseLine
    size_t m_numPieces;

    // Range of values for line slopes
    float m_minM, m_maxM;

    // Range of values for the first line's y intercept
    float m_minB, m_maxB;

    // Range of values for x boundaries between lines
    float m_minX, m_maxX;

public:
    PiecewiseLineSpawner(
        // Filename where training coordinate data is stored in space separeted
        // value format
        const string &dataFilename,
        // The number of intervals in the piecewise line
        size_t numPieces,
        // Range fpr the slope parameter
        float minM, float maxM,
        // Range for the y intercept parameter of the first line
        float minB, float maxB,
        // Range for interval boundary parameters
        float minX, float maxX) :

        m_data(make_shared<DataSet>(dataFilename)),
        m_numPieces(numPieces),
        m_minM(minM), m_maxM(maxM),
        m_minB(minB), m_maxB(maxB),
        m_minX(minX), m_maxX(maxX) { }

    virtual ~PiecewiseLineSpawner() { }

    // Create a new individual as a normalish average of two parents
    virtual IndividualPtr birth(const vector<IndividualPtr> &parents) const override
    {
        const size_t numParams = m_numPieces * 2;
        vector<float> parameters;
        parameters.reserve(numParams);

        for (size_t idx = 0; idx < numParams; idx++)
        {
            float
                minP = parents[0]->at(idx),
                maxP = parents[1]->at(idx);

            if (minP > maxP)
                swap(minP, maxP);

            parameters.push_back(normalish(minP, maxP, 3));
        }

        // the x coordinate boundaries must be in sorted order
        if (m_numPieces > 1)
            std::sort(parameters.begin() + m_numPieces + 1, parameters.end());

        return make_shared<PiecewiseLine>(parameters, m_data);
    }

    // Create a new individual with parameters selected from a uniform distribution
    virtual IndividualPtr random() const override
    {
        vector<float> parameters;
        parameters.reserve(m_numPieces * 2);

        // generate slopes
        for (size_t idx = 0; idx < m_numPieces; idx++)
            parameters.push_back(uniform(m_minM, m_maxM));

        // generate the y intercept of the first line
        parameters.push_back(uniform(m_minB, m_maxB));

        // generate the x coordinate boundaries between each line
        for (size_t idx = 1; idx < m_numPieces; idx++)
            parameters.push_back(uniform(m_minX, m_maxX));

        // the x coordinate boundaries must be in sorted order
        if (m_numPieces > 1)
            std::sort(parameters.begin() + m_numPieces + 1, parameters.end());

        return make_shared<PiecewiseLine>(parameters, m_data);
    }
};


/**
 * Display the help message and exit
 */
void helpAndExit()
{
    cout
        << "Fit a piecewise linear function to a set of noisy points. The "
        << "generation number, score and parameters of the best solution are "
        << "printed for the first and last generations and for each generation "
        << "when a better solution is found."
        << endl << endl
        << "Yes, this could be done by iteratively optimizing the boundaries "
        << "and performing linear regression on the points between the them, "
        << "but the code at least demonstrates the usage of the core classes "
        << "and how to setup    to solve a problem using genetic algorithms."
        << endl;
    exit(1);
}


/**
 * Perform piecewise linear regression with genetic algorithms or display help
 * and exit
 */
int main(int argc, char *argv[])
{
    // look for a request for help, all other arguments are ignored
    for (int idx = 0; idx < argc; idx++)
    {
        if (strcmp(argv[idx], "--help") == 0 || strcmp(argv[idx], "-h") == 0)
            helpAndExit();
    }

    // seed the random number generator
    srand(time(0));

    const size_t numPieces = 3;

    // this object will generate new individuals for the population
    PiecewiseLineSpawner spawner(
        // currently assuming that we're in the project root
        "demos/data/plinear.txt",
        numPieces,
        -100.0f, 100.0f,
        -100.0f, 100.0f,
        // the data points are in [-3,3]
        -3.0f,   3.0f);

    // the container for our solutions. the high ratio of random spawns is to
    // prevent the population from settling into a local min too easily
    Population<float, PiecewiseLine, PiecewiseLineSpawner>
        population(50, 2, 0.25f, 0.25f, spawner);

    runner(population, 100000);
}
